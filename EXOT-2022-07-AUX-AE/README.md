## Autoencoder for CERN-EP-2023-112 (https://arxiv.org/abs/2307.01612) ##

This  example demonstrates how to load the trained autoencoder used in this paper. It provides a simple example of processing 10,000 events converted to rapidity-mass matrices (RMM). The trained autoencoder can be used to estimate acceptances for any  BSM model.  

This repository is a part of the HEPdata entry for this paper https://www.hepdata.net/record/144864 which will be available after publication.
 

## A note on the acceptance calculation for any BSM model

(1) Step 1: Create a BSM model events and convert such events to RMM using
    https://github.com/chekanov/Map2RMM. 

(2) Use events in the form of RMM as inputs for the trained autoencoder located
    in the directory "autoencoder".

(3) Reject events if math.log(xloss) < -9.10. See the example code "analysis.py"
    See the comment in the source code of this file.

## Setup

(1) For Anaconda, you can use the setup file: autoencoder.yml

(2) When setting up using separate packages, use this setup:

```
Numpy version      : 1.21.2
Pandas version     : 1.4.1
Matplotlib version : 3.5.1
Seaborn version    : 0.11.2
Tensorflow version : 2.4.1
```

(3) If you are using LXPLUS, we are providing a simple script which does the setup:

```
source ./setup_lxplus.sh
```

The trained autoencoder for the CERN-EP-2023-112 paper is available from the directory "autoencoder"

# Read and process dummy events

To demonstrate how to load the trained model and process 10,000 events with empty
rapidity-mass matrices (RMM), use this example:

```
python analysis.py
```

This example feeds RMM and applies the trained autoencoder. It also uses file "columns_with_0.txt" to remove invariant masses
from the input which will be our signal region. 

# Generate BSM events and process with the trained autoencoder

This repository also provides the full Pythia8 example in the directory [fullanalysis](https://gitlab.cern.ch/dijetpluslepton/exot-2022-07/-/tree/master/EXOT-2022-07-AUX-AE/fullanalysis?ref_type=heads). The Pythia8 model is used to generate the Sequential-Standard model (SSM) events. Then it creates a ROOT file with the RMM data in the form of a tree (keeping only non-zero values). Then a Python script is used to process this ROOT file, apply the autoencoder trained using the 1% of ATLAS data used in this paper, and fill histograms with 9 invariant masses after applying the 10 pb anomaly region cut. 

Conversion of collision events to RMM is also shown in https://github.com/chekanov/Map2RMM

S.V.Chekanov (ANL) (chekanov@anl.gov)

Dec 5, 2023. The ATLAS Collaboration
