## Autoencoder for CERN-EP-2023-112 (https://arxiv.org/abs/2307.01612) ##
##
# Show how to load autoencoder used for the ATLAS paper
# and process 10,000 events in chinks of 1000 with empty RMM
# The code for RMM is https://github.com/chekanov/Map2RMM
# Use this code to calculate acceptances for any BSM model

import os,sys
import random
os.environ['PYTHONHASHSEED']=str(1)

import numpy
import pandas
import matplotlib
import seaborn
import tensorflow
import pickle
print('Numpy version      :' , numpy.__version__)
print('Pandas version     :' ,pandas.__version__)
print('Matplotlib version :' ,matplotlib.__version__)
print('Seaborn version    :' , seaborn.__version__)
print('Tensorflow version :' , tensorflow.__version__)
import numpy as np
import pandas as pd
import datetime
import tensorflow as tf
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense
import pandas
import math
import seaborn
import tensorflow
import tensorflow as tf

# random seeds fixed
RANDOM_SEED = 101
os.environ['PYTHONHASHSEED']=str(1)
tf.random.set_seed(RANDOM_SEED)
np.random.seed(RANDOM_SEED*2)
random.seed(RANDOM_SEED*3)
print("Use fixed seed=",RANDOM_SEED)
os.environ["OMP_NUM_THREADS"] = "1"
physical_devices = tf.config.list_physical_devices('CPU')
tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)

# total events
NtotInFile=10000

print("-> Run over max events=",NtotInFile)
print("")

maxNumber= 10
maxTypes= 5
mSize=maxTypes*maxNumber+1;

# double lit to keep data for dataframe
columnsX=[]
for i in range(1,mSize*mSize+1):
       columnsX.append("V_"+str(i))
# last column labels the data (put 0) 
# columnsX.append("Label")
df = pd.DataFrame(columns=columnsX)
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)


fj1="autoencoder/models"
loaded_model = tf.keras.models.load_model( fj1 )
print("--> Loaded model from "+fj1)

loaded_model.summary()
loaded_model.compile(optimizer='adam', loss='mse' )

file0="columns_with_0.txt"
print ("Read columns with 0 from ",file0)
dcol0=pd.read_csv(file0,header = None)
col0=dcol0[dcol0.columns[0]]

ntot=0
print ("Start processing..") 
# how many chunks with RMM to process in one go 
evtINchunk=1000

nchunk=0
chunk=0
evt=0
ev=0

# initialize RMM with zeros 
RMM = np.zeros(shape=(evtINchunk, mSize*mSize))
for event in range(10000):
       emptyMatrix = numpy.zeros(shape=(mSize,mSize))
       # we use empty matrix in this example. Here you need to fill RMM from your model
       # flatten
       dataRMM=(emptyMatrix.flatten()).tolist()
       RMM[evt,:]=dataRMM       
       evt=evt+1   # events in chunk 
       ev=ev+1     # events in this file 
       ntot=ntot+1 # all events 
       if (ev ==  NtotInFile or evt%evtINchunk==0):
                     df = pd.DataFrame(data=RMM, columns=columnsX)
                     df=df.drop(col0, axis = 1)
                     RMM_T = df.to_numpy()
                     # get AE prediction
                     predictions = loaded_model.predict( RMM_T )
                     train_loss = tf.keras.losses.mae(predictions, RMM_T).numpy()
                     nle=len(train_loss)
                     if (ev ==  NtotInFile):
                                          nle=ev- nchunk*evtINchunk 
                                          print(" -> Last event ",ev, " from ",NtotInFile, "remaining=",nle)
                     for ch in range(nle):
                            xloss=train_loss[ch]
                            xlog= math.log(xloss) 
                            # here you can reject events with small loss, keeping anomalous events 
                            # For example, remove events with xlog < -9.1 as in this ATLAS paper. 
                            # 
                            # print("Loss=",xlog)
                     # reset
                     RMM = np.zeros(shape=(evtINchunk, mSize*mSize))
                     print("Fill chunk ",nchunk," with ",evtINchunk, " events. Tot=",ev)
                     nchunk=nchunk+1
                     evt=0
print("Total events =",ev);






