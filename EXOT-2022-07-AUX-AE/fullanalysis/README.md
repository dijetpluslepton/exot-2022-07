# Full analysis using Pythia8

This program generates 10,000 Pythia8 events and converts them to the rapidity-mass matrices (RMM, Nucl. Instrum. Meth. A, vol. 931, pp.92, 2019) in the form of a ROOT tree, keeping only non-zero values of RMMs. The output file is in the "out" directory. 
Pythia8 input parameters are in "tev13_A14_WZPrime.py". The generated events correspond to the Sequential-Standard model (BSM) process with W' at 2.25 TeV and Z' at 2 TeV. The analysis code was used in https://arxiv.org/abs/2111.12119 (S.V.Chekanov, W.Hopkins, Universe 2022, 8(10), 494) and was adopted by the ATLAS collaboration in the publication https://arxiv.org/abs/2307.01612 (CERN-EP-2023-112) "Search for new phenomena in two-body invariant mass distributions using unsupervised machine learning for anomaly detection at s√=13 TeV with the ATLAS detector". 
 
To compile and execute this program, define $ROOTSYS (ROOT program), $FASTJET (fastjet library) and $PYTHIADIR (Pythia8 program): 

(1) If you do not have these libraries, one can use the singularity image file (/centos7hepsim.img) from the HepSim repository 
(https://atlaswww.hep.anl.gov/hepsim/doc/doku.php?id=hepsim:dev_singularity): 

```
wget http://atlaswww.hep.anl.gov/hepsim/soft/centos7hepsim.img
```

and then run:

```
singularity exec centos7hepsim.img  bash -l
source /opt/hepsim.sh
```
(2) If you are using LXPLUS, you can also use this setup:

```
source ./setup_lxplus.sh
```

After you setup the environment, then compile main.cxx and run it using:  

```
./A_RUN
```

It fills a ROOT file in the "out" directory. This ROOT file includes a tree with non-zero values of RMM.

If you wish to validate the RMM tree, use the PyROOT script (validate_data.py). This script removes 9 invariant masses which could be your signal region, and makes the image with average values of RMM.

# Processing Pythia8 events with autoencoder

After you make the ROOT file (out/out_pythia8_wzprime.root) with RMMs from Pythia8, now you can process this file using the ATLAS autoencoder.
As usual, you need these libraries:

```
Numpy version      : 1.21.2
Pandas version     : 1.4.1
Matplotlib version : 3.5.1
Seaborn version    : 0.11.2
Tensorflow version : 2.4.1
```

plus pyROOT. Typically, 

```
source ../setup_lxplus.sh
```

should be enough to setup these libraries. Then run this script: 

```
./A_RUN_ANALYSIS_BSM
```

It calls "analysis_root_chunky.py" using the trained autoencoder from the directory ../autoencoder/. It also uses file "../columns_with_0.txt" to remove invariant masses from the input to the autoencoder which will be our signal region. 

The output of this script is the file "wzprime_nominal.root" with the invariant masses after the autoencoder using the 10 pb ATLAS cut (-9.1). 
The output file has several histograms such as the loss distribution (Loss_wzprime_nominal) and the 9 invariant masses in the anomaly region (Mjj_wzprime_nominal, Mbb_wzprime_nominal, etc.). The histogram with the loss values should match reasonably well to the SSM model shown in Fig.1 of https://arxiv.org/abs/2307.01612. Some differences might be expected since the ATLAS results use Pythia8 after the detector simulation.
This example also prints events which have been removed by the autoencoder. 

This example can be used to estimate the acceptance for any arbitrary BSM model. If you use Delphes fast simulation (https://arxiv.org/abs/1307.6346), you need to write a C++ code using same interface function (projectevent) as shown in main.cxx.    

S.V.Chekanov (ANL), chekanov@anl.gov  
 
