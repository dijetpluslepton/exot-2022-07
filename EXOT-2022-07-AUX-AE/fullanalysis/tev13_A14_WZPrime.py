# Pythia8 ATLAS MB Tune A14 with NNPDF23LO tune.
EventsNumber=50000
# sliming the particle record?
ApplyParticleSlim = off
#Collision settings
Random:setSeed = on
Random:seed = 0
Beams:idA = 2212
Beams:idB = 2212
Beams:eCM = 14000.
#physics processes
#HardQCD:all = on
HardQCD:all = off
ParticleDecays:limitTau0 = on
#Makes particles with c*tau>10 mm stable
ParticleDecays:tau0Max = 10
Tune:pp = 14 
Tune:ee = 7
PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed 
PDF:extrapolate = on

SpaceShower:rapidityOrder = on
SigmaProcess:alphaSvalue = 0.140
SpaceShower:pT0Ref = 1.56
SpaceShower:pTmaxFudge = 0.91
SpaceShower:pTdampFudge = 1.05
SpaceShower:alphaSvalue = 0.127
TimeShower:alphaSvalue = 0.127
BeamRemnants:primordialKThard = 1.88
MultipartonInteractions:pT0Ref = 2.09
MultipartonInteractions:alphaSvalue = 0.126
# old method: BeamRemnants:reconnectRange
ColourReconnection:reconnect=on
ColourReconnection:range=1.71
#
# W' (34) -> Z' (32) W
# Keep W' mass at 2.25 TeV and Z' at 2.0 TeV
# allow semileptonic W decays
NewGaugeBoson:ffbar2Wprime = on
34:m0 = 2250 
34:onMode = off
34:addChannel = 1 0.5 103 24 32
# Z' pramaters
32:mMin = 10 
32:mMax = 6000;
32:m0 = 2000.0
32:onMode = off
32:onIfAny = 1 2 3 4 5
# use 10% width (fixed) for Z' 
32:doForceWidth = true
32:mWidth = 20;

# W decay. Assume leptonic channels 
24:onMode = off;
24:onIfAny = 11 13 15;

