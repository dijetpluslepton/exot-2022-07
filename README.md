# EXOT-2022-07

## Additional material for EXOT-2022-07

The AUX material is https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2022-07/ 

This repository includes a file with the trained autoenoder which can be used to calculate the acceptance of the anomaly detection cut for any arbitrary BSM model. 

In addition, it includes the complete Pythia8 example to create truth-level events for a BSM model shown in the ATLAS paper, transform them to the rapidity-mass matrix, and to select events using the autoencoder trained using the 1% of ATLAS data. This example can be used to calculate autoencoder acceptances for any arbitrary BSM model.  

This GIT repository is linked from the HEPdata entry (https://www.hepdata.net/record/144864) as "Resources" -> "Common Resources" -> "GitHub Repository".

S.V.Chekanov (ANL) Email: chekanov@anl.gov 
